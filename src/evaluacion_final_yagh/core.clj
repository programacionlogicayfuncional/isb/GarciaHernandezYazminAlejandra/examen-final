(ns evaluacion-final-yagh.core
  (:gen-class))
(use 'clojure.set)

;;1. Dada una secuencia de sentidos indicar si se regresa al punto de origen:
(defn regresa-al-punto-de-origen?
  [mov]
  (letfn [(f [x] (cond
                   (= x \<) [-1 0]
                   (= x \>) [1 0]
                   (= x \v) [0 -1]
                   (= x \^) [0 1]))
          (g [x] (vector (reduce + (map first x)) (reduce + (map last x))))]
    (= [0 0] (g (map f mov)))))

;;2. Dada n secuencias de sentidos indicar si todas regresan a su propio punto de origen:
(defn regresan-al-punto-de-origen?
  [& args]
  (letfn [(f [x] (cond
                   (= x \<) [-1 0]
                   (= x \>) [1 0]
                   (= x \v) [0 -1]
                   (= x \^) [0 1]))
          (g [x] (vector (reduce + (map first x)) (reduce + (map last x))))
          (h [x] (= [0 0] (g (map f x))))
          (i [xs] (reduce (fn [x y] (and x y)) true xs))]
    (i (into #{} (map h args)))))

;;3. Dada una secuencia de sentidos, regresar la secuencia de sentidos que permita regresar al punto de origen en sentido contrario:
(defn regreso-al-punto-de-origen
  [mov]
  (letfn [(f [x] (cond
                   (= x \<) [-1 0]
                   (= x \>) [1 0]
                   (= x \v) [0 -1]
                   (= x \^) [0 1]))
          (g [x] (vector (reductions + 0 (map first x)) (reductions + 0 (map last x))))
          (h [xs] (map (fn [x y] (vector x y)) (first xs) (last xs)))
          (i [xs] (vector (map - (rest (map first xs)) (map first xs)) (map - (rest (map last xs)) (map last xs))))
          (j [x] (cond
                   (= x [-1 0]) \<
                   (= x [1 0]) \>
                   (= x [0 -1]) \v
                   (= x [0 1]) \^))
          (k [xs] (if (= [0 0] (first xs)) () (map j (h (i xs)))))]
    (k (reverse (h (g (map f mov)))))))

;;4. Dadas 2 secuencias de sentidos, las cuales parten del mismo punto de origen, indicar si ambas terminan en el mismo punto final.
(defn mismo-punto-final?
  [mov-1 mov-2]
  (letfn [(f [x] (cond
                   (= x \<) [-1 0]
                   (= x \>) [1 0]
                   (= x \v) [0 -1]
                   (= x \^) [0 1]))
          (g [x] (vector (reduce + (map first x)) (reduce + (map last x))))]
    (= (g (map f mov-1)) (g (map f mov-2)))))

;;5. Dadas 2 secuencias de sentidos, las cuales parten del mismo punto de origen, indicar cuantas veces coinciden en un mismo punto:
(defn coincidencias
  [mov-1 mov-2]
  (letfn [(f [x] (cond
                   (= x \<) [-1 0]
                   (= x \>) [1 0]
                   (= x \v) [0 -1]
                   (= x \^) [0 1]))
          (g [x] (vector (reductions + 0 (map first x)) (reductions + 0 (map last x))))
          (h [xs] (map (fn [x y] (vector x y)) (first xs) (last xs)))
          (i [] (intersection (into #{} (h (g (map f mov-1)))) (into #{} (h (g (map f mov-2))))))]
    (count (i))))
