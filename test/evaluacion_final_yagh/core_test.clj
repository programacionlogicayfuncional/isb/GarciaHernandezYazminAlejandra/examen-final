(ns evaluacion-final-yagh.core-test
  (:require [clojure.test :refer :all]
            [evaluacion-final-yagh.core :refer :all]))

(deftest regresa-al-punto-de-origen?-test
  (testing "Problema 1, casos true"
    (is (= true (regresa-al-punto-de-origen? "><")))
    (is (= true (regresa-al-punto-de-origen? (list \> \<))))
    (is (= true (regresa-al-punto-de-origen? "v^")))
    (is (= true (regresa-al-punto-de-origen? [\v \^])))
    (is (= true (regresa-al-punto-de-origen? "^>v<")))
    (is (= true (regresa-al-punto-de-origen? (list \^ \> \v \<))))
    (is (= true (regresa-al-punto-de-origen? "<<vv>>^^")))
    (is (= true (regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^]))))
  (testing "Problema 1. casos false"
    (is (= false (regresa-al-punto-de-origen? ">")))
    (is (= false (regresa-al-punto-de-origen? (list \>))))
    (is (= false (regresa-al-punto-de-origen? "<^")))
    (is (= false (regresa-al-punto-de-origen? [\< \^])))
    (is (= false (regresa-al-punto-de-origen? ">>><<")))
    (is (= false (regresa-al-punto-de-origen? (list \> \> \> \< \<))))
    (is (= false (regresa-al-punto-de-origen? [\v \v \^ \^ \^])))))

(deftest regresan-al-punto-de-origen?-test
  (testing "Problema 2, casos true"
    (is (= true (regresan-al-punto-de-origen?)))
    (is (= true (regresan-al-punto-de-origen? [])))
    (is (= true (regresan-al-punto-de-origen? "")))
    (is (= true (regresan-al-punto-de-origen? [] "" (list))))
    (is (= true (regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")))
    (is (= true (regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v)))))
  (testing "Problema 2, casos false"
    (is (= false (regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])))
    (is (= false (regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")))
    (is (= false (regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])))))

(deftest regreso-al-punto-de-origen-test
  (testing "Problema 3"
    (is (= () (regreso-al-punto-de-origen "")))
    (is (= () (regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))))
    (is (= '(\< \< \<) (regreso-al-punto-de-origen ">>>")))
    (is (= '(\< \< \^ \^ \^ \>) (regreso-al-punto-de-origen [\< \v \v \v \> \>])))))

(deftest mismo-punto-final?-test
  (testing "Problema 4, casos true"
    (is (= true (mismo-punto-final? "" [])))
    (is (= true (mismo-punto-final? "^^^" "<^^^>")))
    (is (= true (mismo-punto-final? [\< \< \< \>] (list \< \<))))
    (is (= true (mismo-punto-final? (list \< \v \>) (list \> \v \<)))))
  (testing "Problema 4, casos false"
    (is (= false (mismo-punto-final? "" "<")))
    (is (= false (mismo-punto-final? [\> \>] "<>")))
    (is (= false (mismo-punto-final? [\> \> \>] [\> \> \> \>])))
    (is (= false (mismo-punto-final? (list) (list \^))))))

(deftest coincidencias-test
  (testing "Problema 5"
    (is (= 1 (coincidencias "" [])))
    (is (= 1 (coincidencias (list \< \<) [\> \>])))
    (is (= 2 (coincidencias [\^ \> \> \> \^] ">^^<")))
    (is (= 4 (coincidencias "<<vv>>^>>" "vv<^")))
    (is (= 6 (coincidencias ">>>>>" [\> \> \> \> \>])))
    (is (= 6 (coincidencias [\> \> \> \> \>] (list \> \> \> \> \> \> \^ \^ \^ \^))))))
